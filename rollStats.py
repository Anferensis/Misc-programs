
"""
Written by Albert"Anferensis"Ong
"""

from random import randint


def rollStats(mode = "Normal"):
	""" A function that will automatically roll a character's stats. 
	
	Only accepts one input named "mode".
	"mode" can only be one of two values: "Normal" or "Variant". 
	
	Normal mode calculation: 3d6 + 4
	Variant mode calulation: d30
	"""
	
	# Raises an error if the inputted mode is not "Normal" or "Variant".
	if mode not in ("Normal", "Variant"):
		raise ValueError("Inputted mode must be either be 'Normal' or 'Variant'. ")
	
	
	# A variable that will represent a character's stats. 
	character_stats = ""
	
	# For loops six times, one for each stat. 
	for stat_name in ("Strength:     ", 
					  "Dexterity:    ", 
					  "Constitution: ", 
					  "Intelligence: ", 
					  "Wisdom:       ", 
					  "Charisma:     "):
		
		# If the mode is "Normal"...
		if mode == "Normal":
			
			# Starts with a base value of 4
			stat_value = 4
			
			# Adds three random values between 1 and 6
			# This simulates rolling a 6-sided dice three times. 
			for num in range(3):
				stat_value += randint(1, 6)
		
		# If the mode is "Variant"		
		elif mode == "Variant":
			
			# Generates a random number between 1 and 30
			#  This simulates rolling a 30-sided dice. 
			stat_value = randint(1, 30)
		
		# Formats each stat into a string. 			  
		stat_line = stat_name + str(stat_value) + "\n"
		
		# 
		character_stats += stat_line
	
	
	return character_stats
	

	
#=======================================================================


def main():
	
	character_stats = rollStats()
	
	print("Character Stats: ")
	print("-----------------")
	print(character_stats)
	

if __name__ == "__main__":
	main()
	
	
	
